﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINR.Interpreter.Exceptions
{
    class OperationException : Exception
    {
        public OperationException()
        {
        }

        public OperationException(string message)
        : base(message)
        {
        }

        public OperationException(string message, Exception inner)
        : base(message, inner)
        {
        }

        public OperationException(OperationInfo info) : base(info.ToString())
        {
        }
    }
}
