﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINR.Interpreter.Exceptions
{
    class SectionParseException : Exception
    {
        public SectionParseException()
        {
        }

        public SectionParseException(string message)
        : base(message)
        {
        }

        public SectionParseException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
