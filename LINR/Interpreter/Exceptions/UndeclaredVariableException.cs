﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINR.Interpreter.Exceptions
{
    class UndeclaredVariableException : Exception
    {
        public UndeclaredVariableException()
        {
        }

        public UndeclaredVariableException(string message)
        : base(message)
        {
        }

        public UndeclaredVariableException(string message, Exception inner)
        : base(message, inner)
        {
        }

        public UndeclaredVariableException(OperationInfo info) : base(info.ToString())
        {
        }
    }
}
