﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LINR.Interpreter.Exceptions;

namespace LINR.Interpreter
{
    class InterpreterManager
    {
        private Dictionary<string, float> variables;
        private string[] varDeclaration;
        private string[] operations;
        private char splitchar; 
        int lineMarker;

        public InterpreterManager(string program)
        {
            program = System.Text.RegularExpressions.Regex.Replace(program, @"\t|\n|\r", "");
            string[] sections = program.Split('@');
            if(sections.Length != 2)
            {
                throw new SectionParseException("Must be two code sections to be valid");
            }
            if(sections[0].Length == 0)
            {
                throw new SectionParseException("Program cannot be empty");
            }

            splitchar = sections[0].Substring(0, 1).ToCharArray()[0];
            varDeclaration = sections[0].Split(splitchar);
            operations = sections[1].Split(splitchar);
                

            variables = new Dictionary<string, float>();
            foreach(string variable in varDeclaration)
            {
                if (!string.IsNullOrEmpty(variable))
                {
                    variables.Add(variable, float.NaN);
                }
            }

            operations = RemoveNullEmptyAndWhiteSpace(operations);

            lineMarker = 0;
        }

        private string[] RemoveNullEmptyAndWhiteSpace(string[] args)
        {
            List<string> valid = new List<string>();

            for(int i = 0; i < args.Length; i++)
            {
                if(!string.IsNullOrWhiteSpace(args[i]) && !string.IsNullOrEmpty(args[i]))
                {
                    valid.Add(args[i]);
                }
            }
            return valid.ToArray();
        }

        public void Execute(bool debug = false)
        {
            lineMarker = 0;

            //We skip every lineCount%8th line until we complete the program
            int skipLines = operations.Length%8;
            HashSet<int> completedOperations = new HashSet<int>();
            int iterationTracker = 0;
            int currentOperation = 0;

            if (operations != null)
            {
                try
                {
                    OperationInfo lastInfo;
                    while(completedOperations.Count < operations.Length)
                    {
                        if (!completedOperations.Contains(currentOperation))
                        {
                            lineMarker++;
                            if (!string.IsNullOrEmpty(operations[currentOperation]))
                            {
                                string[] args = operations[currentOperation].Split(' ');
                                Operation operation = GetOperation(args);
                                if (operation.Op != Operations.NUL)
                                {
                                    if (args.Length == 4)
                                    {
                                        lastInfo = DoOp(operation);
                                        lastInfo.LineNo = currentOperation;
                                        if (lastInfo.Success)
                                        {
                                            if (variables.ContainsKey(args[3]))
                                            {
                                                variables[args[3]] = lastInfo.Answer;
                                            }
                                            else
                                            {
                                                throw new OperationException("Invalid var - " + lastInfo.ToString());
                                            }
                                            if (debug)
                                            {
                                                Console.Write(lineMarker + ":- ");
                                                Console.WriteLine(lastInfo.ToString());
                                                foreach (KeyValuePair<string, float> variable in variables)
                                                {
                                                    Console.Write(variable.Key + " : " + variable.Value.ToString() + ", ");
                                                }
                                                Console.WriteLine();
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("WARN - " + lastInfo.ToString());
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("ERR - Did not exec \'" + operations[currentOperation] + "\'");
                                    }
                                }
                            }
                            completedOperations.Add(currentOperation);
                        }
                        currentOperation += skipLines;
                        if(currentOperation >= operations.Length)
                        {
                            currentOperation = ++iterationTracker;
                        }
                    }
                    for (int i = 0; i < operations.Length; i+=skipLines)
                    {
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ERR - " + ex.Message);
                    return;
                }
            }else
            {
                Console.WriteLine("ERR - No program loaded");
                return;
            }
            foreach(KeyValuePair<string, float> variable in variables)
            {
                Console.WriteLine(variable.Key + " : " + variable.Value.ToString());
            }
        }

        private Operation GetOperation(string[] args)
        {
            float valueA;
            float valueB;
            Operation operation;
            Operations opEnum = Operation.OpFromString(args[0]);

            if (IsFloat(args[1]))
            {
                valueA = float.Parse(args[1]);
                if (IsFloat(args[2]))
                {
                    valueB = float.Parse(args[2]);

                    operation = new Operation(opEnum, valueA, valueB);
                }
                else
                {
                    operation = new Operation(opEnum, valueA, args[2]);
                }
            }
            else
            {
                if (IsFloat(args[2]))
                {
                    valueB = float.Parse(args[2]);

                    operation = new Operation(opEnum, args[1], valueB);
                }
                else
                {
                    operation = new Operation(opEnum, args[1], args[2]);
                }
            }
            return operation;
        }

        private bool IsFloat(string testee)
        {
            float value;
            if(float.TryParse(testee, out value))
            {
                return true;
            }
            return false;
        }

        private OperationInfo DoOp(Operation operation)
        {
            float answer;
            float[] values;
            OperationInfo info;
            if (operation.TryGetValues(variables, out values))
            {
                float a = values[0];
                float b = values[1];

                switch (operation.Op)
                {
                    case Operations.ADD:
                        answer = a + b;
                        info = new OperationInfo(true, "ADD");
                        break;
                    case Operations.SUB:
                        answer = a - b;
                        info = new OperationInfo(true, "SUB");
                        break;
                    case Operations.MUL:
                        answer = a * b;
                        info = new OperationInfo(true, "MUL");
                        break;
                    case Operations.DIV:
                        answer = a / b;
                        info = new OperationInfo(true, "DIV");
                        break;
                    case Operations.NUL:
                        answer = float.NaN;
                        info = new OperationInfo(true, "NUL");
                        break;
                    default:
                        answer = float.NaN;
                        info = new OperationInfo(false, "Operation invalid");
                        break;
                }
                info.Answer = answer;
                return info;
            }
            else
            {
                return new OperationInfo(false, operation.Op.ToString() + " failed");
            }
        }
    }
}
