﻿using System;
using System.Collections.Generic;
using LINR.Interpreter.Exceptions;

namespace LINR.Interpreter
{
    class Operation
    {
        Operations op;
        float valueA, valueB;
        string variableA, variableB;
        bool usesVariableValue;

        public Operations Op
        {
            get
            {
                return op;
            }
        }

        public Operation(Operations op, float valA, float valB)
        {
            this.op = op;
            this.valueA = valA;
            this.valueB = valB;
            usesVariableValue = false;
        }

        public Operation(Operations op, float valA, string varB)
        {
            this.op = op;
            this.valueA = valA;
            this.valueB = float.NaN;
            this.variableB = varB;
            usesVariableValue = true;
        }

        public Operation(Operations op, string varA, float valB)
        {
            this.op = op;
            this.variableA = varA;
            this.valueA = float.NaN;
            this.valueB = valB;
            usesVariableValue = true;
        }

        public Operation(Operations op, string varA, string varB)
        {
            this.op = op;
            this.variableA = varA;
            this.variableB = varB;
            this.valueA = float.NaN;
            this.valueB = float.NaN;
            usesVariableValue = true;
        }

        public bool TryGetValues(Dictionary<string,float> variableValues, out float[] values)
        {
            float[] workingValues = new float[2];
            if (!usesVariableValue)
            {
                workingValues[0] = valueA;
                workingValues[1] = valueB;
                values = workingValues;
                return true;
            }
            else
            {
                if (variableA != null && variableB != null)
                {
                    if (variableValues.TryGetValue(variableA, out workingValues[0]))
                    {
                        if (variableValues.TryGetValue(variableB, out workingValues[1]))
                        {
                            values = workingValues;
                            return true;
                        }
                    }
                }
                else
                {
                    if (variableA != null)
                    {
                        if (variableValues.TryGetValue(variableA, out workingValues[0]))
                        {
                            workingValues[1] = valueB;
                            values = workingValues;
                            return true;
                        }
                        else
                        {
                            throw new UndeclaredVariableException(new OperationInfo(false, op.ToString() + " failed with undeclared var " + variableA));
                        }
                    }
                    else if (variableB != null)
                    {
                        if (variableValues.TryGetValue(variableB, out workingValues[2]))
                        {
                            workingValues[0] = valueA;
                            values = workingValues;
                            return true;
                        }
                        else
                        {
                            throw new UndeclaredVariableException(new OperationInfo(false, op.ToString() + " failed with undeclared var " + variableB));
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException("INTERNAL - Operation object was in invalid state when parsing variables");
                    }
                }
            }
            values = null;
            return false;
        }

        public static Operations OpFromString(string operation)
        {
            Operations op = Operations.NUL;
            if(Enum.TryParse(operation, out op))
            {
                return op;
            }
            else
            {
                throw new OperationException("Invalid value for operation conversion - " + operation);
            }
        }
    }
}
