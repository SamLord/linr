﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINR.Interpreter
{
    class OperationInfo
    {
        private string info;
        private bool success;

        public string Info
        {
            get
            {
                return info;
            }
        }

        public bool Success
        {
            get
            {
                return success;
            }
        }

        public int LineNo
        {
            get;set;
        }

        public float Answer
        {
            get; set;
        }

        public OperationInfo(bool successful, string extraInfo)
        {
            info = extraInfo;
            success = successful;
            LineNo = -1;
            Answer = float.NaN;
        }

        public override string ToString()
        {
            return info + "; Success:" + Success.ToString().ToUpper() + ", Answer: " + Answer + " at " + LineNo;
        }
    }
}
