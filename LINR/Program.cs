﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LINR.Interpreter;

namespace LINR
{
    class Program
    {
        private static InterpreterManager interpreter;

        static void Main(string[] args)
        {
            Console.WriteLine("LINR >>");
            while (true)
            {
                Console.WriteLine(">");
                string readIn = Console.ReadLine();
                args = readIn.Split(' ');
                switch (args[0].ToLower())
                {
                    case ("run"):
                        interpreter = null;
                        Program.Run(args);
                        break;
                    case ("exec"):
                    case ("execute"):
                        string inlineFile = "";
                        for (int i = 1; i < args.Length; i++)
                        {
                            inlineFile += args[i];
                            if(i != args.Length - 1)
                            {
                                inlineFile += " ";
                            }
                        }
                        interpreter = new InterpreterManager(inlineFile);
                        interpreter.Execute();
                        break;
                    case ("debug"):
                        Program.Run(args, true);
                        break;
                    case ("quit"):
                    case ("exit"):
                        return;
                    default:
                        Console.WriteLine("No such command - run <file>, exec <code>, quit");
                        break;
                }
            }
        }

        private static void Load(string[] args)
        {
            if(args.Length > 1)
            {
                InitInterpreter(args[1]);
            }
            else
            {
                Console.WriteLine("No file supplied");
            }
        }

        private static void Run(string[] args, bool debug = false)
        {
            Console.WriteLine(args.Length);
            if(args.Length > 1)
            {
                InitInterpreter(args[1]);
                if (interpreter != null)
                {
                    interpreter.Execute(debug);
                }
            }
            else if(args.Length == 1 && interpreter != null)
            {
                interpreter.Execute(debug);
            }
        }

        private static void InitInterpreter(string programPath)
        {
            try
            {
                interpreter = new InterpreterManager(System.IO.File.ReadAllText(programPath));
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERR - " + ex.Message);
            }
        }
    }
}
